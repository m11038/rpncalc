// testStack.c by *****

#include <stdio.h>
#include "stack.h"
#include "testCommon.h"


void testInitStack() {
    stack myStack;
    testStart("initStackのテストは大丈夫？");
    myStack.top = 0; // 適当な値にセット
    initStack(&myStack, N);
    assert_equals_int(myStack.top, N); // sp が最終値より後ろになっているか
    testEnd();
}

void testTopValue(void) {
    stack myStack;
    testStart("topValueのテストは大丈夫？");
    initStack(&myStack,N);
    /* stack が空なら NaN を返す */
    assert_equals_int(isnan(topValue(&myStack)), 1);
    /* stack が空でなければスタックのさしている数字を返す */
    myStack.top = 0;
    myStack.data[0] = 5.0;
    assert_equals_int(isnan(topValue(&myStack)), 0); // not NaN
    assert_equals_double(topValue(&myStack), 5.0);
    testEnd();
}


void testpush(){
    stack myStack;
    testStart("pushのテストは大丈夫？");
    
    initStack(&myStack,N);
    myStack.top = N;
    
    push(&myStack,5.0);
    assert_equals_double(topValue(&myStack),5.0);
    assert_equals_int(myStack.top,N-1);
    
    push(&myStack,3);
    assert_equals_double(topValue(&myStack),3.0);
    assert_equals_int(myStack.top,N-2);
    
    testEnd();
}


void testpull(){
    stack myStack;
    myStack.top = N;
    
    testStart("pullのテストは大丈夫？");
        push(&myStack,5.0); 
        push(&myStack,3.0);
    
        assert_equals_double(pull(&myStack),3.0);
        assert_equals_int(myStack.top,N-1);        
        assert_equals_double(pull(&myStack),5.0);
        assert_equals_int(myStack.top,N);
        pull(&myStack);
        assert_equals_double(isnan(pull(&myStack)),1);
        assert_equals_int(myStack.top,N);
    testEnd();
}


void testprintStack(){
    stack myStack;    

    testStart("printStackのテストは大丈夫？");
        initStack(&myStack,N);
        push(&myStack,5.0); 
        push(&myStack,3.0); 
        printStack(&myStack);
    testEnd();
}

void testaddStack(){
    stack myStack;
    testStart("addStackのテストは大丈夫？");
    
        initStack(&myStack,N);
    
        push(&myStack,5.0); 
        push(&myStack,3.0);
        addStack(&myStack);
        assert_equals_int(myStack.top,N-1);
        assert_equals_double(topValue(&myStack),8.0);
    
        addStack(&myStack);
        assert_equals_int(myStack.top,N-1);
        assert_equals_double(isnan(pull(&myStack)),1);  
    
        push(&myStack,18.0); 
        push(&myStack,2.0);
        push(&myStack,4.4);
    
        addStack(&myStack);
        assert_equals_double(topValue(&myStack),6.4);
        addStack(&myStack);
        assert_equals_double(topValue(&myStack),24.4);
    
    testEnd();
}

void testsubStack(){
    
    stack myStack;
    initStack(&myStack,N);
    
    testStart("subStackのテストは大丈夫？");
    
        push(&myStack,5.0); 
        push(&myStack,3.0);
        subStack(&myStack);
        assert_equals_int(myStack.top,N-1);
        assert_equals_double(topValue(&myStack),2.0);
    
        subStack(&myStack);
        assert_equals_int(myStack.top,N-1);
        assert_equals_double(isnan(pull(&myStack)),1);

        push(&myStack,1.0);
        push(&myStack,5.0); 
        push(&myStack,4.0);
        push(&myStack,19.0);
    
        subStack(&myStack);
        assert_equals_double(topValue(&myStack),-15.0);
        subStack(&myStack);
        assert_equals_double(topValue(&myStack),20.0);    
        subStack(&myStack);
        assert_equals_double(topValue(&myStack),-19.0);    
    testEnd();
}

void testmulStack(){
    
    stack myStack;
    initStack(&myStack,N);
    
    testStart("mulStackのテストは？");
    push(&myStack,5.0); 
    push(&myStack,3.0);
    mulStack(&myStack);
    assert_equals_int(myStack.top,N-1);
    assert_equals_double(topValue(&myStack),15.0);
    
    mulStack(&myStack);
    assert_equals_int(myStack.top,N-1);
    assert_equals_double(isnan(pull(&myStack)),1); 
    testEnd();
}

void testdivStack(){
    
    stack myStack;
    initStack(&myStack,N);
    
    testStart("divStackのテストは大丈夫？");
    
    push(&myStack,15.0); 
    push(&myStack,3.0);
    divStack(&myStack);
    assert_equals_int(myStack.top,N-1);
    assert_equals_double(topValue(&myStack),5.0);
    
    divStack(&myStack);
    assert_equals_int(myStack.top,N-1);
    assert_equals_double(isnan(pull(&myStack)),1);
    
    push(&myStack,99.0);    
    push(&myStack,99.0); 
    push(&myStack,6.0);
    push(&myStack,3.0);
    divStack(&myStack);
    assert_equals_double(topValue(&myStack),2.0);
    divStack(&myStack);
    assert_equals_double(topValue(&myStack),49.5);    
    divStack(&myStack);
    assert_equals_double(topValue(&myStack),2.0);
    
    testEnd();
}

void testsinStack(){
    
    stack myStack;
    initStack(&myStack,N);
    
    testStart("sinStackのテストは大丈夫？？");
    
        push(&myStack,15.0); 
        push(&myStack,M_PI);
        push(&myStack,155); 
        push(&myStack,3.1415926/4);
        sinStack(&myStack);
        assert_equals_double(topValue(&myStack),1/sqrt(2));
    
    testEnd();
}


void testcosStack(){
    
    stack myStack;
    initStack(&myStack,N);
    
    testStart("cosStackのテストは大丈夫？");
    
    push(&myStack,15.0); 
    push(&myStack,M_PI);
    push(&myStack,155);
    push(&myStack,2*3.1415926/6);
    cosStack(&myStack);
    assert_equals_double(topValue(&myStack),0.5);

    testEnd();
}


int main(){
    testInitStack();
    testTopValue();
    testpush();
    testpull();
    testprintStack();
    testaddStack();
    testsubStack();
    testmulStack();
    testdivStack();
    testsinStack();
    testcosStack();
    return 0;
}
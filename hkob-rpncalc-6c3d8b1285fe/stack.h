// stack.h by *****

#define N 10000

typedef struct {
    double data[N];
    int top;
} stack;

void initStack(stack *sp, int n);
double topValue(stack *sp);
void push(stack *sp,double x);
double  pull(stack  *sp);
void printStack(stack *sp);
void addStack(stack *sp);
void subStack(stack *sp);
void mulStack(stack *sp);
void divStack(stack *sp);
void sinStack(stack *sp);
void cosStack(stack *sp);
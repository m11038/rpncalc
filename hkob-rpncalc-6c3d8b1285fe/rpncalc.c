#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"
#include "testCommon.h"


#define D 256


int main() {
    
    char line[D],line2[D];
    double data,broke,k;
    stack lineStack;
    initStack(&lineStack,N);
    broke=5;
 
    while (broke != 10) {
    
        fgets(line,N,stdin);
        data = atof(line);

        if (strcmp(line, "+\n") == 0) {
            addStack(&lineStack);
            //printf("足し算だよ"); 
        }else if(strcmp(line, "-\n") == 0){
            subStack(&lineStack);
            //printf("ひき算だよ"); 
        }else if(strcmp(line, "*\n") == 0){
            mulStack(&lineStack);
            //printf("かけ算だよ"); 
        }else if(strcmp(line, "/\n") == 0){
            divStack(&lineStack);
            //printf("割り算だよ"); 
        }else if(strcmp(line, "sin\n") == 0){
            *(lineStack).data[*(lineStack).top-1] = 0.0 ;
            printf("sinするけど...rad表示で計算しますか？y/n"); 
            fgets(line,N,stdin);
            if (strcmp(line, "y\n") == 0) {
                    sinStack(&lineStack);
            }else{
                    sinStack(&lineStack);
            }
            
        }else if(strcmp(line, "sin2\n") == 0){
            
            printf("後から打ち込んでsinするけど良いですか？...rad表示で計算しますか？y/n"); 
            fgets(line2,N,stdin);
            if (strcmp(line2, "y\n") == 0) {
                printf("rad表示で入力してください");
                fgets(line,N,stdin);
                sinStack(&lineStack);
            }else{
                printf("角度表示で入力してください");
                fgets(line,N,stdin);
                sinStack(&lineStack);
            }
            
        }else if(strcmp(line, "cos\n") == 0){
            printf("cosが出来ないです。");
        }else if(strcmp(line, "p\n") == 0){
            k = pull(&lineStack);
            printf("プルした数字は%fですね。\n",k);
        }else if(strcmp(line, "e\n") == 0){
            broke = 10;
        }else if(strcmp(line, "r\n") == 0){
            initStack(&lineStack,N);
            printf("スタックをリセットしました。yes or no\n");
        }else if(strcmp(line, "0\n") != 0 && data == 0){
            printf("あなた今、変な文字を入力しましたわよ？");
            //printf("気合いだっ！");
           //printf("修行するぞ！"); 
        }else{
           // printf("数字だよ。プッシュ。");
            push(&lineStack,data);
        }
        
      //  printf("%f\n",broke);
        printStack(&lineStack);
        printf("\n  \n");
    }


    
	return 0;
}
